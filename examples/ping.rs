#![no_std]
#![no_main]

#[allow(unused_imports)]
use panic_semihosting;

use stm32l0xx_hal as hal;
use hal::prelude::*;
use hal::delay::Delay;
use hal::rcc::{Config as RccConfig, PLLSource, PLLDiv, PLLMul};
use hal::spi::{Spi, MODE_0};

// use radio_sx127x::prelude::*;
use embedded_spi::wrapper::Wrapper as SpiWrapper;

#[macro_use]
use rtfm::app;

#[app(device=stm32l0xx_hal::pac)]
const APP: () = {

    #[init]
    fn init() {

        let mut rcc = device.RCC.freeze(
            RccConfig::pll(PLLSource::HSI16, PLLMul::Mul6, PLLDiv::Div3)
        );

        let gpioa = device.GPIOA.split(&mut rcc);
        let gpiob = device.GPIOB.split(&mut rcc);
        let gpioc = device.GPIOC.split(&mut rcc);

        let sck = gpiob.pb3.into_floating_input();
        let miso = gpioa.pa6.into_floating_input();
        let mosi = gpioa.pa7.into_floating_input();

        let spi1 = Spi::spi1(
            device.SPI1,
            (sck, miso, mosi),
            MODE_0, 
            1u32.mhz(),
            &mut rcc
        );

        let cs = gpioa.pa15.into_push_pull_output();
        let reset = gpioc.pc0.into_push_pull_output();
        let mut spi_hal = SpiWrapper::new(
            spi1, 
            cs, 
            (),
            (),
            reset,
            Delay::new(core.SYST, rcc.clocks),
            );

        // let radio_config = Config::default();
        // let radio = Sx127x::new(spi_hal, &radio_config);

    }

};