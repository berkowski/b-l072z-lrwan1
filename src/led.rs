
use core::ops;

use embedded_hal::Direction;

use crate::hal::prelude::*;
use crate::hal::gpio::gpiob::{PB, PB5, PB6, PB7};
use crate::hal::gpio::gpioa::{PA, PA5};
use crate::hal::gpio::{Output, PushPull};

pub type LD1 = PB5<Output<PushPull>>;
pub type LD2 = PA5<Output<PushPull>>;
pub type LD3 = PB6<Output<PushPull>>;
pub type LD4 = PB7<Output<PushPull>>;

enum LedEnum {
    Pa(PA<Output<PushPull>>),
    Pb(PB<Output<PushPull>>),
}

impl OutputPin for LedEnum {
    type Error = ();
    fn set_high(&mut self) -> Result<(), Self::Error> {
        match self {
            LedEnum::Pa(x) => x.set_high(),
            LedEnum::Pb(x) => x.set_high(),
        }
    }

    fn set_low(&mut self) -> Result<(), Self::Error> {
        match self {
            LedEnum::Pa(x) => x.set_low(),
            LedEnum::Pb(x) => x.set_low(),
        }
    }
}

impl ToggleableOutputPin for LedEnum {
    type Error = ();

    fn toggle(&mut self) -> Result<(), Self::Error> {
        match self {
            LedEnum::Pa(x) => x.toggle(),
            LedEnum::Pb(x) => x.toggle()
        }
    }
}

pub struct Led {
    led: LedEnum,
}

impl Led {
    pub fn off(&mut self) -> Result<(), ()> {
       self.led.set_low() 
    }

    pub fn on(&mut self) -> Result<(), ()> {
       self.led.set_high() 
    }

    pub fn toggle(&mut self) -> Result<(), ()> {
        self.led.toggle()
    }
}

impl Into<Led> for PB5<Output<PushPull>> {
    fn into(self) -> Led {
        Led {
            led: LedEnum::Pb(self.downgrade())
        }
    }
}

impl Into<Led> for PA5<Output<PushPull>> {
    fn into(self) -> Led {
        Led {
            led: LedEnum::Pa(self.downgrade())
        }
    }
}

impl Into<Led> for PB6<Output<PushPull>> {
    fn into(self) -> Led {
        Led {
            led: LedEnum::Pb(self.downgrade())
        }
    }
}

impl Into<Led> for PB7<Output<PushPull>> {
    fn into(self) -> Led {
        Led {
            led: LedEnum::Pb(self.downgrade())
        }
    }
}

pub struct Leds {
    leds: [Led; 4],
}

impl Leds {
    pub fn new(ld1: LD1, ld2: LD2, ld3: LD3, ld4: LD4) -> Self {
        Leds{ leds: [ ld1.into(), ld2.into(), ld3.into(), ld4.into() ] }
    }
}

impl ops::Deref for Leds {
    type Target = [Led];

    fn deref(&self) -> &[Led] {
        &self.leds
    }
}

impl ops::DerefMut for Leds {
    fn deref_mut(&mut self) -> &mut [Led] {
        &mut self.leds
    }
}

impl ops::Index<usize> for Leds {
    type Output = Led;

    fn index(&self, i: usize) -> &Led {
        &self.leds[i]
    }
}

impl ops::Index<Direction> for Leds {
    type Output = Led;

    fn index(&self, d: Direction) -> &Led {
        &self.leds[d as usize]
    }
}

impl ops::IndexMut<usize> for Leds {
    fn index_mut(&mut self, i: usize) -> &mut Led {
        &mut self.leds[i]
    }
}

impl ops::IndexMut<Direction> for Leds {
    fn index_mut(&mut self, d: Direction) -> &mut Led {
        &mut self.leds[d as usize]
    }
}