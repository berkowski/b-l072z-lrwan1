#![no_std]
#![no_main]

#[allow(unused_imports)]
use panic_semihosting;

use stm32l0xx_hal as hal;
use hal::prelude::*;
use hal::delay::Delay;
use hal::rcc::{Config as RccConfig};

use b_l072z_lrwan1::led::Leds;

use rtfm::app;

#[app(device=stm32l0xx_hal::pac)]
const APP: () = {

    static mut LEDS: Leds = ();
    static mut DELAY: Delay = ();

    #[init]
    fn init() -> init::LateResources {

        let mut rcc = device.RCC.freeze(
            RccConfig::hsi16()
        );

        let gpioa = device.GPIOA.split(&mut rcc);
        let gpiob = device.GPIOB.split(&mut rcc);

        let leds = Leds::new(
            gpiob.pb5.into_push_pull_output(),
            gpioa.pa5.into_push_pull_output(),
            gpiob.pb6.into_push_pull_output(),
            gpiob.pb7.into_push_pull_output()
        );

        let delay = Delay::new(core.SYST, rcc.clocks);

        init::LateResources { LEDS: leds, DELAY: delay }
    }

    #[idle(resources = [LEDS, DELAY])]
    fn idle() -> ! {
        let mut i: usize = 0;
        loop {
            resources.LEDS[i].toggle().unwrap();
            resources.DELAY.delay_ms(500u32);
            i = (i + 1) % resources.LEDS.len()
        }
    }

};
